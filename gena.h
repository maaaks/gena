#ifndef GENA_H
#define GENA_H

#include "gena/commons.h"

#include "gena/arg.h"
#include "gena/class.h"
#include "gena/constructor.h"
#include "gena/destructor.h"
#include "gena/field.h"
#include "gena/filepair.h"
#include "gena/forwarddeclaration.h"
#include "gena/include.h"
#include "gena/method.h"
#include "gena/namespace.h"
#include "gena/type.h"

#endif // GENA_H
