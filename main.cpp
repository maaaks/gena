#include <QtCore/QString>
#include <QtCore/QTextStream>
#include "gena.h"

int main(int argc, char** argv)
{
	Gena::FilePair filePair("test");
	filePair.hIncludes << new Gena::Include(Gena::SystemInclude, "something.h");
	filePair.cppIncludes << new Gena::Include("something_else.h");
	
	Gena::Namespace math("Math");
	filePair << &math;
	
	math << new Gena::ForwardDeclaration("Future");
	
	Gena::Class number("Number");
	math << &number;
	number << new Gena::Field(Gena::Static, Gena::Public, "int", "_value", "0");
	number << new Gena::Constructor(Gena::Public, {"int value=1"}, {"_value(value)"}, "");
	number << new Gena::Destructor(Gena::Public, "");
	number << new Gena::Method(Gena::Public, "int", "value", {}, "return _value");
	number << new Gena::Method(Gena::Public, "QString", "toString", {}, "return QString::number(_value);");
	number << new Gena::Method(Gena::Public, "Meta", "meta", {}, "return Meta(*this);");
	
	Gena::Class meta("Meta");
	number << &meta;
	meta << new Gena::Field(Gena::Public, "bool", "isNegative");
	meta << new Gena::Field(Gena::Public, "bool", "isPositive");
	meta << new Gena::Field(Gena::Public, "bool", "isZero");
	meta << new Gena::Constructor(Gena::Public, {"Math::Number number"},
		"isNegative = (number.value() < 0);\n"
		"isPositive = (number.value() > 0);\n"
		"isZero = (number.value() == 0);\n"
	);
	
	Gena::Class one("One", {"public Class"});
	math << &one;
	one << new Gena::Constructor(Gena::Public, {}, {"Number(1)"}, "");
	
	Gena::Class two("Two", {"public Class"});
	math << &two;
	two << new Gena::Constructor(Gena::Public, {}, {"Number(2)"}, "");
	
	Gena::Class three("Three", {"public Class"});
	math << &three;
	three << new Gena::Constructor(Gena::Public, {}, {"Number(3)"}, "");
	
	QTextStream(stdout) << filePair.generateDeclaration();
	QTextStream(stdout) << "\n==========================================\n\n";
	QTextStream(stdout) << filePair.generateImplementation();
}
