#ifndef GENA_FILEPAIR_H
#define GENA_FILEPAIR_H

#include <QtCore/QStringList>

namespace Gena {

class ClassOrNamespace;
class Class;
class ForwardDeclaration;
class Include;
class Namespace;

class FilePair
{
public:
	QString name;
	QList<Include*> hIncludes;
	QList<Include*> cppIncludes;
	QStringList hUsingNamespaces;
	QStringList cppUsingNamespaces;
	QList<ForwardDeclaration*> forwardDeclarations;
	QList<ClassOrNamespace*> classesAndNamespaces;
	
	QString headerName;
	
	FilePair(const QString& name);
	
	QString generateDeclaration() const;
	QString generateImplementation() const;
	
	FilePair& operator<<(Class* cl);
	FilePair& operator<<(Namespace* ns);
	FilePair& operator<<(ForwardDeclaration* forwardDeclaration);
	FilePair& operator<<(Include* hInclude);
};
}

#endif // GENA_FILEPAIR_H
