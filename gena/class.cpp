#include "class.h"

#include <QtCore/QMap>
#include "commons.h"
#include "constructor.h"
#include "destructor.h"
#include "field.h"
#include "method.h"

using namespace Gena;

Class::Class(const QString& name, const QStringList& bases) :
	name(name), bases(bases)
{
}

/////////////////////////////////////////////////

QString Class::generateDeclaration() const
{
	// Create three empty parts of the declaration code.
	// Each member will be written to one part.
	QMap<Access,QString> parts;
	parts[Private] = "";
	parts[Protected] = "";
	parts[Public] = "";
	
	// Inner classes
	foreach (Class* c, innerClasses)
		parts[Public] += pad("  ") + c->generateDeclaration() + "\n";
	
	// Fields
	foreach (Field* f, fields)
		parts[f->access] += pad("  ") + f->generateDeclaration();
	
	// Constructors
	foreach (Constructor* c, constructors)
		parts[c->access] += pad("  ") + c->generateDeclaration(name);
	
	// Destructor
	if (destructor != 0)
		parts[destructor->access] += pad("  ") + destructor->generateDeclaration(name);
	
	// Methods
	foreach (Method* m, methods)
		parts[m->access] += pad("  ") + m->generateDeclaration();
	
	// Construct the whole code
	
	QString code;
	if (!parts[Private].trimmed().isEmpty())
		code += parts[Private];
	if (!parts[Protected].trimmed().isEmpty())
		code += "\nprotected:\n" + parts[Protected];
	if (!parts[Public].trimmed().isEmpty())
		code += "\npublic:\n" + parts[Public];
	code = code.trimmed();
	
	QString opening = "class " + name;
	if (!bases.isEmpty())
		opening += " : " + bases.join(", ");
	opening += "\n{\n";
	
	QString closing = "};\n\n";
	if (!code.isEmpty())
		closing.prepend("\n");
	
	return opening + code + closing;
}

QString Class::generateImplementation(QString fullName) const
{
	if (fullName.isEmpty())
		fullName = name;
	
	QString cpp;
	
	// Static fields
	bool staticFieldsWritten = false;
	foreach (Field* f, fields) {
		QString fieldCode = f->generateImplementation(fullName+"::"+f->name);
		if (!fieldCode.isEmpty()) {
			cpp += fieldCode;
			staticFieldsWritten = true;
		}
	}
	if (staticFieldsWritten)
		cpp += "\n";
	
	// Constructors
	foreach (Constructor* c, constructors)
		cpp += c->generateImplementation(name, fullName);
	
	// Destructors
	if (destructor != 0)
		cpp += destructor->generateImplementation(name, fullName);
	
	// Methods
	foreach (Method* m, methods)
		cpp += m->generateImplementation(name, fullName);
	
	// Inner classes
	foreach (Class* c, innerClasses)
		cpp += c->generateImplementation(fullName+"::"+c->name);
	
	return cpp;
}

Class& Class::operator<<(Class* innerClass)
{
	innerClasses << innerClass;
	return *this;
}

Class& Class::operator<<(Constructor* constructor)
{
	constructors << constructor;
	return *this;
}

Class& Class::operator<<(Destructor* destructor)
{
	if (this->destructor == 0)
		delete this->destructor;
	this->destructor = destructor;
	return *this;
}

Class& Class::operator<<(Field* field)
{
	fields << field;
	return *this;
}

Class& Class::operator<<(Method* method)
{
	methods << method;
	return *this;
}
