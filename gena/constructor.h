#ifndef GENA_CONSTRUCTOR_H
#define GENA_CONSTRUCTOR_H

#include <QtCore/QStringList>
#include "method.h"

namespace Gena {

class Constructor : public Method
{
public:
	QStringList bases;
	
	Constructor(Access access, const QList<Arg>& arguments, const QString& body);
	Constructor(Access access, const QList<Arg>& arguments, const QStringList& bases, const QString& body);
	
	QString generateDeclaration(const QString& className) const;
	QString generateImplementationTitle(const QString& className, QString classFullName=QString()) const;
};

}

#endif // GENA_CONSTRUCTOR_H
