#include "method.h"

#include <QtCore/QStringList>

using namespace Gena;

Method::Method(Access access, const Type type, const QString& name, const QList<Arg>& arguments, const QString& body) :
	access(access), type(type), name(name), arguments(arguments), body(body)
{
}

Method::Method(IsStatic isStatic, Access access, const Type type, const QString& name, const QList<Arg>& arguments, const QString& body) :
	isStatic(isStatic), access(access), type(type), name(name), arguments(arguments), body(body)
{
}

QString Method::generateArgList(Arg::WithOrWithoutExpr wowe) const
{
	QStringList args;
	foreach (const Arg& arg, arguments)
		args << arg.generate(wowe);
	return args.join(", ");
}

QString Method::generateImplementationTitle(const QString& className, QString classFullName) const
{
	if (classFullName.isEmpty())
		classFullName = className;
	return type.get() + " " + classFullName + "::" + name + "(" + generateArgList(Arg::WithoutExpr) + ")";
}

QString Method::generateDeclaration() const
{
	QString code;
	
	code += (isStatic == Static) ? "static " : "";
	code += type.get() + " " + name;
	code += "(" + generateArgList(Arg::WithExpr) + ");";
	code += "\n";
	
	return code;
}

QString Method::generateImplementation(const QString& className, QString classFullName) const
{
	return generateImplementationTitle(className, classFullName) + "\n{\n"
		+ (!body.isEmpty()
			? "  " + body.trimmed().split("\n").join("\n  ") + "\n"
			: "")
		+ "}\n\n";
}
