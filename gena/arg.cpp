#include "arg.h"

using namespace Gena;

Arg::Arg(Type type, const QString& name, const QString& expr) :
	type(type), name(name), expr(expr)
{
}

Arg::Arg(QString code)
{
	QString typeAndName = code.section("=", 0, 0);
	
	type = typeAndName.section(" ", 0, -2, QString::SectionSkipEmpty);
	name = typeAndName.section(" ", -1, -1, QString::SectionSkipEmpty);
	expr = code.section("=", 1, -1).trimmed();
}

Arg::Arg(const char* code) :
	Arg(QString(code))
{
}

QString Arg::generate(WithOrWithoutExpr wowe) const
{
	if (wowe==WithoutExpr || expr.isEmpty())
		return type.get() + " " + name;
	else
		return type.get() + " " + name + "=" + expr;
}
