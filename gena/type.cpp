#include "type.h"

#include "class.h"

using namespace Gena;

Type::Type(const QString &str) :
	str(str), cl(0)
{
}

Type::Type(const char* str) :
	str(str), cl(0)
{
}

Type::Type(Class* cl) :
	str(QString()), cl(cl)
{
}

Type::Type(Class& cl) :
	str(QString())
{
	this->cl = &cl;
}

QString Type::get() const
{
	return str.isEmpty() ? cl->name : str;
}
