#include "constructor.h"

using namespace Gena;

Constructor::Constructor(Access access, const QList<Arg>& arguments, const QString& body) :
	Method(access, QString(), {}, arguments, body)
{
}

Constructor::Constructor(Access access, const QList<Arg>& arguments, const QStringList& bases, const QString& body) :
	Method(access, QString(), QString(), arguments, body), bases(bases)
{
}

QString Constructor::generateDeclaration(const QString& className) const
{
	return className + "(" + generateArgList(Arg::WithExpr) + ");\n";
}

QString Constructor::generateImplementationTitle(const QString& className, QString classFullName) const
{
	if (classFullName.isEmpty())
		classFullName = className;
	
	QString title = classFullName + "::" + className + "(" + generateArgList(Arg::WithoutExpr) + ")";
	if (!bases.isEmpty()) {
		title += " :\n  " + bases.join(",\n  ");
	}
	return title;
}
