#ifndef COMMONS_H
#define COMMONS_H

#include <QtCore/QString>

namespace Gena
{

class Class;
class Namespace;

/**
 * Access modifiers for class members.
 */
enum Access {
	Private,
	Protected,
	Public,
};

/**
 * Values for defining whether a class member is static or non-static.
 */
enum IsStatic {
	NonStatic,
	Static,
};

/**
 * Type of include, used in Include class.
 */
enum IncludeType {
	SystemInclude,
	LocalInclude
};

/**
 * Aux class for padding lines.
 * Usage:
 * @code
 * QString output = pad("  ") + input;
 * @endcode
 */
class pad
{
	QString pre;
public:
	pad(const QString& pre);
	QString operator+(const QString& string);
};

/**
 * A universal class that can represent either a Class or a Namespace.
 */
class ClassOrNamespace
{
public:
	enum Type {ThisIsClass, ThisIsNamespace};
	
private:
	void* _ptr;
	Type _type;
	
public:
	ClassOrNamespace(Class* cl);
	ClassOrNamespace(Namespace* ns);
	
	Type type() const;
	Class* getClass() const;
	Namespace* getNs() const;
};

}

#endif // COMMONS_H
