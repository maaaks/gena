#include "namespace.h"

#include "class.h"
#include "commons.h"
#include "forwarddeclaration.h"

using namespace Gena;

Namespace::Namespace(const QString& name) :
	name(name)
{
}

QString Namespace::generateDeclaration() const
{
	QString h;
	
	// Forward declarations
	if (!forwardDeclarations.isEmpty()) {
		foreach (ForwardDeclaration* f, forwardDeclarations)
			h += f->generateDeclaration() + "\n";
		h += "\n";
	}
	
	// Classes and Namespaces
	foreach (ClassOrNamespace* item, classesAndNamespaces)
		if (item->type() == ClassOrNamespace::ThisIsClass)
			h += item->getClass()->generateDeclaration() + "\n";
		else
			h += item->getNs()->generateDeclaration() + "\n";
	
	if (!h.trimmed().isEmpty()) {
		h = pad("  ") + h.trimmed();
		h = "namespace " + name + "\n{\n" + h + "}\n";
		return h;
	} else {
		return "";
	}
}

QString Namespace::generateImplementation(QString fullName) const
{
	QString prefix = fullName.isEmpty() ? name+"::" : fullName+"::";
	
	QString h;
	foreach (ClassOrNamespace* item, classesAndNamespaces) {
		if (item->type() == ClassOrNamespace::ThisIsClass) {
			Class* c = item->getClass();
			h += c->generateImplementation(prefix + c->name);
		} else {
			Namespace* ns = item->getNs();
			h += ns->generateImplementation(prefix + ns->name);
		}
	}
	return h;
}

Namespace& Namespace::operator<<(Namespace* ns)
{
	classesAndNamespaces << new ClassOrNamespace(ns);
	return *this;
}

Namespace& Namespace::operator<<(ForwardDeclaration* forwardDeclaration)
{
	foreach (ForwardDeclaration* decl, forwardDeclarations)
		if (decl->name == forwardDeclaration->name)
			return *this;
	
	forwardDeclarations << forwardDeclaration;
	return *this;
}

Namespace& Namespace::operator<<(Class* cl)
{
	classesAndNamespaces << new ClassOrNamespace(cl);
	return *this;
}
