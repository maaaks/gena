#ifndef GENA_DESTRUCTOR_H
#define GENA_DESTRUCTOR_H

#include <QtCore/QList>
#include <QtCore/QString>
#include "method.h"

namespace Gena {

class Destructor : public Method
{
public:
	Destructor(Access access, const QString& body=QString());
	
	QString generateDeclaration(const QString& className) const;
	QString generateImplementationTitle(const QString& className, QString classFullName=QString()) const;
};

}

#endif // GENA_DESTRUCTOR_H
