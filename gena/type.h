#ifndef GENA_TYPE_H
#define GENA_TYPE_H

#include <QtCore/QString>

namespace Gena {

class Class;

/**
 * An argument's or a method's type can be either a string ("int", "QVariant", etc.)
 * or a pointer to an existing @c Gena::Class object.
 * Any of these types can be stored in a @c Gena::Type object.
 */
class Type
{
public:
	QString str;
	Class* cl;
	
	Type(const QString& str="void");
	Type(const char* str);
	Type(Class* cl);
	Type(Class& cl);
	
	QString get() const;
};

}

#endif // GENA_TYPE_H
