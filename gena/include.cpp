#include "include.h"

using namespace Gena;

Include::Include(const QString& path) :
	includeType(LocalInclude), path(path)
{
}

Include::Include(IncludeType includeType, const QString& path) :
	includeType(includeType), path(path)
{
}

QString Include::generate() const
{
	if (includeType == SystemInclude)
		return "#include <" + path + ">\n";
	else
		return "#include \"" + path + "\"\n";
}
