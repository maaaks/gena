#ifndef GENA_CLASS_H
#define GENA_CLASS_H

#include <QtCore/QStringList>
#include "../gena.h"

namespace Gena {

class Constructor;
class Destructor;
class Field;
class Method;

class Class
{
public:
	QString name;
	QStringList bases;
	QList<Class*> innerClasses;
	QList<Constructor*> constructors;
	Destructor* destructor = 0;
	QList<Field*> fields;
	QList<Method*> methods;
	
	Class(const QString& name, const QStringList& bases={});
	
	QString generateDeclaration() const;
	QString generateImplementation(QString fullName=QString()) const;
	
	Class& operator<<(Class* innerClass);
	Class& operator<<(Constructor* constructor);
	Class& operator<<(Destructor* destructor);
	Class& operator<<(Field* field);
	Class& operator<<(Method* method);
};

}

#endif // GENA_CLASS_H
