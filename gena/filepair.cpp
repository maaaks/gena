#include "filepair.h"

#include "class.h"
#include "forwarddeclaration.h"
#include "include.h"
#include "namespace.h"

using namespace Gena;

FilePair::FilePair(const QString& name) :
	name(name)
{
	headerName = name.toLower() + ".h";
}

QString FilePair::generateDeclaration() const
{
	QString h;
	h += "#ifndef " + name.toUpper() + "_H\n";
	h += "#define " + name.toUpper() + "_H\n\n";
	
	if (!hIncludes.isEmpty()) {
		foreach (Include* include, hIncludes)
			h += include->generate();
		h += "\n";
	}
	
	if (!hUsingNamespaces.isEmpty()) {
		foreach (QString usingNamespace, hUsingNamespaces)
			h += "using namespace " + usingNamespace + ";\n";
		h += "\n";
	}
	
	// Forward declarations
	if (!forwardDeclarations.isEmpty()) {
		foreach (ForwardDeclaration* f, forwardDeclarations)
			h += f->generateDeclaration() + "\n";
		h += "\n";
	}
	
	// Classes and Namespaces
	foreach (ClassOrNamespace* item, classesAndNamespaces)
		if (item->type() == ClassOrNamespace::ThisIsClass)
			h += item->getClass()->generateDeclaration() + "\n";
		else
			h += item->getNs()->generateDeclaration() + "\n";
	
	h += "\n#endif // " + name.toUpper() + "_H\n";
	return h;
}

QString FilePair::generateImplementation() const
{
	QString cpp;
	cpp += "#include \"" + headerName + ".h\"\n\n";
	
	if (!cppIncludes.isEmpty()) {
		foreach (Include* include, cppIncludes)
			cpp += include->generate();
		cpp += "\n";
	}
	
	if (!cppUsingNamespaces.isEmpty()) {
		foreach (QString usingNamespace, cppUsingNamespaces)
			cpp += "using namespace " + usingNamespace + ";\n";
		cpp += "\n";
	}
	
	foreach (ClassOrNamespace* item, classesAndNamespaces)
		if (item->type() == ClassOrNamespace::ThisIsClass)
			cpp += item->getClass()->generateImplementation();
		else
			cpp += item->getNs()->generateImplementation();
	
	return cpp;
}

FilePair& FilePair::operator<<(Class* cl)
{
	classesAndNamespaces << new ClassOrNamespace(cl);
	return *this;
}

FilePair& FilePair::operator<<(Namespace* ns)
{
	classesAndNamespaces << new ClassOrNamespace(ns);
	return *this;
}

FilePair& FilePair::operator<<(ForwardDeclaration* forwardDeclaration)
{
	foreach (ForwardDeclaration* decl, forwardDeclarations)
		if (decl->name == forwardDeclaration->name)
			return *this;
	
	forwardDeclarations << forwardDeclaration;
	return *this;
}

FilePair& FilePair::operator<<(Include* hInclude)
{
	hIncludes << hInclude;
	return *this;
}
