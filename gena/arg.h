#ifndef GENA_ARGUMENT_H
#define GENA_ARGUMENT_H

#include <QtCore/QString>
#include "type.h"

namespace Gena {

class Class;

class Arg
{
public:
	enum WithOrWithoutExpr {
		WithExpr,
		WithoutExpr
	};
	
	Type type;
	QString name;
	QString expr;
	
	Arg(Type type, const QString& name, const QString& expr=QString());
	
	/**
	 * This constructor allows to create arguments from strings like "int i=2".
	 */
	Arg(QString code);
	Arg(const char* code);
	
	QString generate(WithOrWithoutExpr wowe) const;
};

}

#endif // GENA_ARGUMENT_H
