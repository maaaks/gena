#include "destructor.h"

#include <QtCore/QStringList>

using namespace Gena;

Destructor::Destructor(Access access, const QString& body) :
	Method(access, QString(), QString(), {}, body)
{
}

QString Destructor::generateDeclaration(const QString& className) const
{
	return "~" + className + "();\n";
}

QString Destructor::generateImplementationTitle(const QString& className, QString classFullName) const
{
	if (classFullName.isEmpty())
		classFullName = className;
	
	return classFullName + "::~" + className + "()";
}
