#include "field.h"

using namespace Gena;

Field::Field(Access access, const Type& type, const QString& name, const QString& expression) :
	access(access), type(type), name(name), expression(expression)
{
}

Field::Field(IsStatic isStatic, Access access, const Type& type, const QString& name, const QString& expression) :
	isStatic(isStatic), access(access), type(type), name(name), expression(expression)
{
}

QString Field::generateDeclaration() const
{
	if (isStatic == Static)
		return "static " + type.get() + " " + name + ";\n";
	else if (expression.isEmpty())
		return type.get() + " " + name + ";\n";
	else
		return type.get() + " " + name + " = " + expression + ";\n";
}

QString Field::generateImplementation(QString fullName) const
{
	if (isStatic == Static && !expression.isEmpty())
		return type.get() + " " + fullName + " = " + expression + ";\n";
	else
		return "";
}
