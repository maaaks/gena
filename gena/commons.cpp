#include "commons.h"

using namespace Gena;

pad::pad(const QString &pre) :
	pre(pre)
{
}

QString pad::operator+(const QString& string)
{
	return pre + string.trimmed().replace("\n", "\n"+pre) + "\n";
}

ClassOrNamespace::ClassOrNamespace(Class* cl) :
	_type(ThisIsClass), _ptr(cl)
{
}

ClassOrNamespace::ClassOrNamespace(Namespace* ns) :
	_type(ThisIsNamespace), _ptr(ns)
{
}

ClassOrNamespace::Type ClassOrNamespace::type() const
{
	return _type;
}

Class* ClassOrNamespace::getClass() const
{
	return (Class*)_ptr;
}

Namespace* ClassOrNamespace::getNs() const
{
	return (Namespace*)_ptr;
}
