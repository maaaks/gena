#ifndef GENA_NAMESPACE_H
#define GENA_NAMESPACE_H

#include <QtCore/QList>
#include <QtCore/QString>


namespace Gena {

class Class;
class ClassOrNamespace;
class ForwardDeclaration;

class Namespace
{
public:
	QString name;
	QList<ForwardDeclaration*> forwardDeclarations;
	QList<ClassOrNamespace*> classesAndNamespaces;
	
	Namespace(const QString& name);
	
	QString generateDeclaration() const;
	QString generateImplementation(QString fullName=QString()) const;
	
	Namespace& operator<<(Namespace* ns);
	Namespace& operator<<(ForwardDeclaration* forwardDeclaration);
	Namespace& operator<<(Class* cl);
};

}

#endif // GENA_NAMESPACE_H
