#ifndef GENA_INCLUDE_H
#define GENA_INCLUDE_H

#include <QtCore/QString>
#include "commons.h"

namespace Gena {

class Include
{
public:
	IncludeType includeType;
	QString path;
	
	Include(const QString& path);
	Include(IncludeType includeType, const QString& path);
	
	QString generate() const;
};

}

#endif // GENA_INCLUDE_H
