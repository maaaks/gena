#ifndef GENA_METHOD_H
#define GENA_METHOD_H

#include <QtCore/QList>
#include <QtCore/QString>
#include "arg.h"
#include "commons.h"

namespace Gena {

/**
 * Stores a method of a class.
 * 
 * Let's look at this method declaration: void Person::go(Place place, int speed=1).
 * The method consists of:
 * @li name ("go"),
 * @li list of arguments ("place" and "speed"), where each argument has a type
 *		and some of arguments may have default values,
 * @li return type ("void").
 * 
 * Beside this, the method has its body.
 * 
 * To create the method, we should do this:
 * @code
 * Gena::Method method("go");
 * method.arguments << "Place place";
 * method.arguments << Gena::Argument("int", "speed", "1");
 * method.body = "// TODO: implement go()\n";
 * @endcode
 */
class Method
{
protected:
	/**
	 * Generates list of arguments to be placed into brackets.
	 * 
	 * Example with expressions:
	 *   "int a=1, int b=2"
	 * Example without expressions:
	 *   "int a, int b"
	 * 
	 * Brackets aren't included in the result.
	 */
	QString generateArgList(Arg::WithOrWithoutExpr wowe) const;
	
	/**
	 * Generates title for implementation.
	 * This method is overriden in Constructor and Destructor.
	 * 
	 * Example:
	 *   "void doSomething(int a, int b)"
	 */
	virtual QString generateImplementationTitle(const QString& className, QString classFullName=QString()) const;
	
public:
	Access access = Private;
	IsStatic isStatic = NonStatic;
	Type type;
	QString name;
	QList<Arg> arguments;
	QString body;
	
	Method(Access access, Type type, const QString& name, const QList<Arg>& arguments, const QString& body);
	Method(IsStatic isStatic, Access access, Type type, const QString& name, const QList<Arg>& arguments, const QString& body);
	
	QString generateDeclaration() const;
	QString generateImplementation(const QString& className, QString classFullName=QString()) const;
};

}

#endif // GENA_METHOD_H
