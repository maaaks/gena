#ifndef GENA_FIELD_H
#define GENA_FIELD_H

#include <QtCore/QString>
#include "commons.h"
#include "type.h"

namespace Gena {
class Field
{
public:
	Access access = Private;
	IsStatic isStatic = NonStatic;
	Type type;
	QString name;
	QString expression;
	
	Field(Access access, const Type& type, const QString& name, const QString& expression=QString());
	Field(IsStatic isStatic, Access access, const Type& type, const QString& name, const QString& expression=QString());
	
	QString generateDeclaration() const;
	QString generateImplementation(QString fullName) const;
};
}

#endif // GENA_FIELD_H
