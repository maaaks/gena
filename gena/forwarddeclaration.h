#ifndef GENA_FORWARDDECLARATION_H
#define GENA_FORWARDDECLARATION_H

#include <QtCore/QString>

namespace Gena {
class ForwardDeclaration
{
public:
	QString name;
	
	ForwardDeclaration(const QString& name);
	
	QString generateDeclaration() const;
};
}

#endif // GENA_FORWARDDECLARATION_H
