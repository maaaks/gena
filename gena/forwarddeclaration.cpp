#include "forwarddeclaration.h"

using namespace Gena;

ForwardDeclaration::ForwardDeclaration(const QString& name) :
	name(name)
{
}

QString ForwardDeclaration::generateDeclaration() const
{
	return "class " + name + ";";
}
